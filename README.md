"Warcrime", a flamethrower weapon addon for Deathstrider.

Comes with three ammotypes: fuel, which burns your enemies, liquid nitrogen, which freezes monsters solid, and acid, which poisons the environment and your foes (don't forget to bring a radsuit.)

As of latest Deathstrider master, no longer requires [OPFAX](https://gitlab.com/outrageous-productions/deathstrider/opfax) to be loaded before this addon, as fluid ammo/mag bases have been ported into Deathstrider.
